package main

import (
  "os"
  "fmt"
  "strconv"
  "bufio"
  "strings"
)

// Error check for invalid operations with io
func check(e error) {
  if e != nil {
    panic(e)
  }
}

// Read in a graph in edge list form from a file at file_name and
// return a graph in adjacency list form using a ragged array
func build_graph(file_name string) [][]int {
  // get input file
  file, err := os.Open(file_name)
  check(err)

  graph := [][]int{}
  scanner := bufio.NewScanner(bufio.NewReader(file))

  // read in the graph edges
  for scanner.Scan() {
    tokens := strings.Split(scanner.Text(), " ")

    from, err := strconv.Atoi(tokens[0])
    check(err)

    to, err := strconv.Atoi(tokens[1])
    check(err)

    for len(graph) < from || len(graph) < to {
      graph = append(graph, []int{})
    }

    // assume that the graph is undirected for the BK algorithm
    if !contains(graph[from - 1], to - 1) {
      graph[from - 1] = append(graph[from - 1], to - 1)
      graph[to - 1] = append(graph[to - 1], from - 1)
    }
  }

  return graph
}

// Helper function to print all nodes in a graph
func print_nodes(nodes []int) {
  fmt.Print("[")
  for i, node := range nodes {
    fmt.Print(node + 1)
    if i + 1 != len(nodes) {
      fmt.Print(" ")
    }
  }
  fmt.Print("]")
}

// Helper function to print all graphs in a collection of graphs
func print_graphs(graphs [][]int) {
  for i, graph := range graphs {
    print_nodes(graph)
    if i + 1 != len(graphs) {
      fmt.Print(", ")
    }
  }
}

// Helper contains function from https://stackoverflow.com/questions/10485743/contains-method-for-a-slice
func contains(values []int, value int) bool {
    for _, element := range values {
        if value == element {
            return true
        }
    }
    return false
}

// Helper min function from https://stackoverflow.com/questions/27516387/what-is-the-correct-way-to-find-the-min-between-two-integers-in-go
func min(a, b int) int {
    if a < b {
        return a
    }
    return b
}

// Searches for all maximal cliques using the basic Bron-Kerbosch algorithm
func find_cliques(R, P, X []int, graph [][]int) [][]int {
  cliques := [][]int{}

  // if there are no nodes to check and no nodes have been excluded,
  // this is a maximal clique
  if len(P) == 0 && len(X) == 0 {
    return append(cliques, R)
  }

  // iterate through all nodes
  for i := 0; i < len(P); {
    node := P[i]

    // add the current node to the clique
    nextR := append(R, node)

    // remove the current node from the potential and exclusion sets
    nextP := []int{}
    nextX := []int{}
    for _, neighbor := range graph[node] {
      if contains(P, neighbor) {
        nextP = append(nextP, neighbor)
      }
      if contains(X, neighbor) {
        nextX = append(nextX, neighbor)
      }
    }

    // add any cliques such that k > 1 that have been found
    for _, clique := range find_cliques(nextR, nextP, nextX, graph) {
      if len(clique) > 1 {
        cliques = append(cliques, clique);
      }
    }

    // remove the current node from the potential set and add
    // it to the exclusion set
    P = append(P[:i], P[i+1:]...)
    X = append(X, node)
  }

  return cliques
}

// finds all communities within a set of cliques
func find_communities(cliques [][]int) [][][]int {
  communities := [][][]int{}
  for range cliques {
    communities = append(communities, [][]int{})
  }

  // iterate through each pair of cliques (C1, C2) and check if at least
  // k - 1 = min(|C1|, |C2|) nodes match
  for i, clique1 := range cliques {
    for _, clique2 := range cliques {
      k := min(len(clique1), len(clique2)) - 1
      count := 0
      for _, node := range clique2 {
        if contains(clique1, node) {
          count++
        }
      }
      if count == k {
        communities[i] = append(communities[i], clique2)
      }
    }
  }
  return communities
}

func main() {
  // check usage
  if len(os.Args) != 2 {
    fmt.Println("usage: clique_percolation <input-file>")
    os.Exit(0)
  }

  // get input file
  graph := build_graph(os.Args[1])

  // populate initial node list
  nodes := make([]int, len(graph))
  for i := 0; i < len(nodes); i++ { nodes[i] = i }

  // find all cliques
  cliques := find_cliques([]int{}, nodes, []int{}, graph)
  communities := find_communities(cliques)

  // print out communities
  for i, community := range communities {
    if len(community) > 1 {
      // print central clique of community
      fmt.Print("Community around ")
      print_nodes(cliques[i])
      fmt.Print(": ")

      // print neighboring cliques
      print_graphs(community)

      fmt.Println()
    }
  }
}
